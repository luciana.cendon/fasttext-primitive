from d3m import index
from d3m.metadata.base import ArgumentType
from d3m.metadata.pipeline import Pipeline, PrimitiveStep

# Common Primitives
from common_primitives.dataset_to_dataframe import DatasetToDataFramePrimitive
from common_primitives.denormalize import DenormalizePrimitive
from common_primitives.extract_columns_semantic_types import ExtractColumnsBySemanticTypesPrimitive
from common_primitives.text_reader import TextReaderPrimitive

# Pipeline
pipeline = Pipeline()
pipeline.add_input(name='inputs')

# Step 0: Dataset Denormalizer
step_0 = PrimitiveStep(primitive_description=DenormalizePrimitive.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline.add_step(step_0)

# Step 1: DatasetToDataFrame
step_1 = PrimitiveStep(primitive_description=DatasetToDataFramePrimitive.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline.add_step(step_1)

# Step 2: Convert filename to text
step_2 = PrimitiveStep(primitive_description=TextReaderPrimitive.metadata.query())
step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_2.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE, data=True)
step_2.add_hyperparameter(name='return_result', argument_type=ArgumentType.VALUE, data='replace')
step_2.add_output('produce')
pipeline.add_step(step_2)

# Step 3: Extract Attributes (Feature Extraction)
step_3 = PrimitiveStep(primitive_description=ExtractColumnsBySemanticTypesPrimitive.metadata.query())
step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.2.produce')
step_3.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE, data=True)
step_3.add_output('produce')
step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE, data=['https://metadata.datadrivendiscovery.org/types/Attribute'] )
pipeline.add_step(step_3)

# Step 3: Extract Targets
step_4 = PrimitiveStep(primitive_description=ExtractColumnsBySemanticTypesPrimitive.metadata.query())
step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_4.add_output('produce')
step_4.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE, data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'] )
pipeline.add_step(step_4)

attributes = 'steps.3.produce'
targets    = 'steps.4.produce'

# Step 4: FastText Classification
step_4 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.natural_language_processing.BAG_OF_TRICKS.Fasttext'))
step_4.add_argument(name='inputs',  argument_type=ArgumentType.CONTAINER,  data_reference=attributes)
step_4.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference=targets)
step_4.add_output('produce')
pipeline.add_step(step_4)

# Step 5: Construct pipeline predictions output
step_5 = PrimitiveStep(primitive=index.get_primitive('d3m.primitives.data_transformation.construct_predictions.Common'))
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference="steps.4.produce")
step_5.add_argument(name='reference', argument_type=ArgumentType.CONTAINER, data_reference="steps.1.produce")
step_5.add_output('produce')
pipeline.add_step(step_5)

# Final Output
pipeline.add_output(name='output predictions', data_reference='steps.5.produce')

# print(pipeline.to_json())
with open('./pipeline.json', 'w') as write_file:
    write_file.write(pipeline.to_json(indent=4, sort_keys=False, ensure_ascii=False))
