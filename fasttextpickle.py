from fasttext import load_model
import tempfile


class PickleableFastText(object):
    def __init__(self, model):
        self._wrapped_model = model

    def __getattr__(self, key):
        return getattr(self._wrapped_model, key)

    def __getstate__(self):
        with tempfile.NamedTemporaryFile(suffix='.bin') as tmp:
            self._wrapped_model.save_model(tmp.name)
            tmp.flush()
            tmp.seek(0, 0)
            return tmp.read()

    def __setstate__(self, state):
        with tempfile.NamedTemporaryFile(suffix='.bin') as tmp:
            tmp.write(state)
            tmp.flush()
            self._wrapped_model = load_model(tmp.name)
