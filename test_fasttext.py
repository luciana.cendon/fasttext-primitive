import unittest
import pickle
import numpy as np
import os
import pandas as pd
import fasttext_prim

from d3m.metadata import base as metadata_base
from d3m import container
from pandas.util.testing import assert_frame_equal
from common_primitives import dataset_to_dataframe, column_parser, text_reader

def extract_text(fname) -> str:
    #dspath = "/Users/cendon/Desktop/D3M/fasttext_primitive/datasets/Jido_1061/JIDO_SOHR_Articles_1061_dataset/media"
    dspath = "/Users/cendon/Desktop/D3M/fasttext_primitive/datasets/Jido_8569/JIDO_SOHR_Tab_Articles_8569_dataset/media"
    path = os.path.join(dspath, fname)
    try:
        return pd.read_csv(path, sep='\n', header=None)[0].str.cat()
    except pd.errors.EmptyDataError:
        # No columns to parse from file
        return ''

#dataset_doc_path = "/Users/cendon/Desktop/D3M/fasttext_primitive/datasets/Jido_1061/JIDO_SOHR_Articles_1061_dataset/datasetDoc.json"
dataset_doc_path = "/Users/cendon/Desktop/D3M/fasttext_primitive/datasets/Jido_8569/JIDO_SOHR_Tab_Articles_8569_dataset/datasetDoc.json"
dataset = container.Dataset.load(dataset_uri="file://{}".format(dataset_doc_path))
hyperparams_class = dataset_to_dataframe.DatasetToDataFramePrimitive.metadata.query()['primitive_code']['class_type_arguments']['Hyperparams']
primitive = dataset_to_dataframe.DatasetToDataFramePrimitive(hyperparams=hyperparams_class.defaults())
call_metadata = primitive.produce(inputs=dataset)

dataframe = call_metadata.value
column_parser_hyperparams = column_parser.Hyperparams.defaults()
column_parser_primitive = column_parser.ColumnParserPrimitive(hyperparams=column_parser_hyperparams)
parsed_dataframe = column_parser_primitive.produce(inputs=dataframe).value

parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/Target')
parsed_dataframe.metadata = parsed_dataframe.metadata.add_semantic_type((metadata_base.ALL_ELEMENTS, 1), 'https://metadata.datadrivendiscovery.org/types/TrueTarget')

parsed_dataframe = parsed_dataframe
ground_truth = parsed_dataframe['articleofinterest']

# Making the dataframe look like the one from runtime
parsed_dataframe['filename'] = parsed_dataframe['article']
parsed_dataframe['article'] = parsed_dataframe['article'].apply(extract_text)
parsed_dataframe = parsed_dataframe.loc[:, ['d3mIndex', 'articleofinterest', 'article', 'filename']]
parsed_dataframe.set_axis(['d3mIndex', 'articleofinterest', 'filename', 'filename'], axis=1, inplace=True)

train_set = targets = parsed_dataframe

semantic_types_to_remove = set(['https://metadata.datadrivendiscovery.org/types/TrueTarget', 'https://metadata.datadrivendiscovery.org/types/SuggestedTarget'])
semantic_types_to_add = {'https://metadata.datadrivendiscovery.org/types/PredictedTarget'}

class TestFastTextPrimitive(unittest.TestCase):

    def create_learner(self, hyperparams):
        learner = fasttext_prim.FastText(hyperparams=hyperparams,
                                         temporary_directory="/Users/cendon/Desktop/D3M/fasttext_primitive/fasttext_prim/tmp")
        return learner

    def set_training_data_on_learner(self, learner, **args):
        learner.set_training_data(**args)

    def set_data(self, hyperparams):
        semantic_type = hyperparams.get("use_semantic_types")
        if semantic_type:
            return {"inputs": train_set, "outputs": targets}
        else:
            return {"inputs": parsed_dataframe.select_columns([3, 2]),
                    "outputs": parsed_dataframe.select_columns([1])}

    def basic_fit(self, hyperparams):
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)
        self.set_training_data_on_learner(learner, **training_data_args)

        learner.fit()

        assert len(learner._training_indices) > 0

        output = learner.produce(inputs=training_data_args.get("inputs"))

        return output, learner, training_data_args

    def pickle(self, hyperparams):

        output, learner, training_data_args = self.basic_fit(hyperparams)

        params = learner.get_params()
        learner.set_params(params=params)

        model = pickle.dumps(learner)
        new_clf = pickle.loads(model)
        new_output = new_clf.produce(inputs=training_data_args.get("inputs"))
        assert_frame_equal(new_output.value, output.value)

    def get_transformed_indices(self, learner):
        return learner._target_column_indices

    def new_return_checker(self, output, indices):
        input_target = train_set.select_columns(list(indices))
        for i in range(len(output.columns)):
            input_semantic_types = input_target.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types")
            output_semantic_type = set(
                output.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            transformed_input_semantic_types = set(input_semantic_types) - semantic_types_to_remove
            transformed_input_semantic_types = transformed_input_semantic_types.union(semantic_types_to_add)
            assert output_semantic_type == transformed_input_semantic_types

    def append_return_checker(self, output, indices):
        for i in range(len(train_set.columns)):
            input_semantic_types = set(train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types

        self.new_return_checker(
            output.value.select_columns(list(range(len(train_set.columns), len(output.value.columns)))),
            indices)

    def replace_return_checker(self, output, indices):
        for i in range(len(train_set.columns)):
            if i in indices:
                continue
            input_semantic_types = set(train_set.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            output_semantic_type = set(
                output.value.metadata.query((metadata_base.ALL_ELEMENTS, i)).get("semantic_types"))
            assert output_semantic_type == input_semantic_types

        self.new_return_checker(output.value.select_columns(list(indices)), indices)

    def test_with_semantic_types(self):
        hyperparams = fasttext_prim.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.basic_fit(hyperparams)
        self.pickle(hyperparams)

    def test_without_semantic_types(self):
        hyperparams = fasttext_prim.Hyperparams.defaults()
        self.basic_fit(hyperparams)
        self.pickle(hyperparams)

    '''def test_f1score(self):
        from sklearn.metrics import f1_score
        hyperparams = fasttext_prim.Hyperparams.defaults()
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)
        self.set_training_data_on_learner(learner, **training_data_args)

        learner.fit()

        assert len(learner._training_indices) > 0

        # Note: For this to run, need to make produce return the output array
        # Resulting F1 score for Jido1061=0.958
        output, y_pred = learner.produce(inputs=training_data_args.get("inputs"))
        f1_score = f1_score(ground_truth.astype(int), y_pred['articleofinterest'])
        print ('F1 Score', f1_score)'''

    '''def test_with_new_return_result(self):
        hyperparams = fasttext_prim.Hyperparams.defaults().replace({"return_result": 'new', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.new_return_checker(output.value, indices)

    def test_with_append_return_result(self):
        # check for adding filename
        hyperparams = fasttext_prim.Hyperparams.defaults().replace({"return_result": 'append', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.append_return_checker(output, indices)

    def test_with_replace_return_result(self):
        hyperparams = fasttext_prim.Hyperparams.defaults().replace({"return_result": 'replace', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.replace_return_checker(output, indices)'''

if __name__ == '__main__':
    unittest.main()


